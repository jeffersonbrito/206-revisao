package br.itau.com.cartoes.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class Cartao {
  @Id
  private String numero;
  @NotNull
  @Enumerated(EnumType.STRING)
  private Bandeira bandeira;
  @NotBlank
  @Length(min = 4, max = 4)
  @JsonProperty(access = Access.WRITE_ONLY)
  private String senha;
  private boolean ativo;
  
  @ManyToOne
  private Cliente cliente;
  
  public String getNumero() {
    return numero;
  }
  
  public void setNumero(String numero) {
    this.numero = numero;
  }
  
  public Bandeira getBandeira() {
    return bandeira;
  }
  
  public void setBandeira(Bandeira bandeira) {
    this.bandeira = bandeira;
  }

  public boolean getAtivo() {
    return ativo;
  }

  public void setAtivo(boolean ativo) {
    this.ativo = ativo;
  }

  public String getSenha() {
    return senha;
  }

  public void setSenha(String senha) {
    this.senha = senha;
  }

  public Cliente getCliente() {
    return cliente;
  }

  public void setCliente(Cliente cliente) {
    this.cliente = cliente;
  }
}
