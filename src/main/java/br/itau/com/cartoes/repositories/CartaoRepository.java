package br.itau.com.cartoes.repositories;

import org.springframework.data.repository.CrudRepository;

import br.itau.com.cartoes.models.Cartao;

public interface CartaoRepository extends CrudRepository<Cartao, String>{}
