package br.itau.com.cartoes.services;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.itau.com.cartoes.exceptions.ValidacaoException;
import br.itau.com.cartoes.models.Cartao;
import br.itau.com.cartoes.models.Lancamento;
import br.itau.com.cartoes.repositories.LancamentoRepository;

@Service
public class LancamentoService {
  @Autowired
  private LancamentoRepository lancamentoRepository;
  
  @Autowired
  private CartaoService cartaoService;

  public Lancamento criar(Lancamento lancamento) {
    Cartao cartao = lancamento.getCartao();
    
    Optional<Cartao> optional = cartaoService.buscar(cartao.getNumero());
    
    if(!optional.isPresent()) {
      throw new ValidacaoException("cartao", "Cartão não encontrado");
    }

    if(!optional.get().getAtivo()) {
        throw new ValidacaoException("cartao", "Cartão não ativo");
    }
    
    lancamento.setHorario(LocalDateTime.now());
    
    return lancamentoRepository.save(lancamento);
  }
  
  public Iterable<Lancamento> buscarTodasPorNumeroDoCartao(String numero){
    return lancamentoRepository.findAllByCartao_numero(numero);
  }
}
