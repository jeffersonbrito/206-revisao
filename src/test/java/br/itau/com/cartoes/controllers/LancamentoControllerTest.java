package br.itau.com.cartoes.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.itau.com.cartoes.models.Cartao;
import br.itau.com.cartoes.models.Lancamento;
import br.itau.com.cartoes.services.LancamentoService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = LancamentoController.class)
public class LancamentoControllerTest {
  @Autowired
  private MockMvc mockMvc;
  
  @MockBean
  private LancamentoService lancamentoService;
  
  private ObjectMapper mapper = new ObjectMapper();
  
  private Cartao cartao;
  private String numeroCartao = "1234156718901230";
  private BigDecimal valor = BigDecimal.valueOf(10.0);
  private String descricao = "uma descricao";
  
  @Before
  public void preparar() {
    cartao = new Cartao();
    cartao.setNumero(numeroCartao);
  }
  
  @Test
  @WithMockUser(username = "1")
  public void deveCriarUmLancamento() throws Exception {
	  cartao.setAtivo(true);
	  
	  when(lancamentoService.criar(Mockito.any(Lancamento.class))).then(answer -> answer.getArgument(0));
	  
	  HashMap<String, Object> lancamentoMap = new HashMap<>();
	  lancamentoMap.put("cartao", cartao);
	  lancamentoMap.put("valor", valor);
	  lancamentoMap.put("descricao", descricao);
	
	  String lancamentoJson = mapper.writeValueAsString(lancamentoMap);
	  
	  mockMvc.perform(post("/lancamento")
	              .contentType(MediaType.APPLICATION_JSON_UTF8)
	              .content(lancamentoJson))
	  			.andExpect(status().isCreated())
	  			.andExpect(content().string(containsString("id")))
	  			.andExpect(content().string(containsString("horario")));
  }
  
  @Test
  @WithMockUser(username = "1")
  public void deveValidarDadosDeLancamento() throws Exception {
	  cartao.setAtivo(true);
	  
	  when(lancamentoService.criar(Mockito.any(Lancamento.class))).then(answer -> answer.getArgument(0));
	  
	  HashMap<String, Object> lancamentoMap = new HashMap<>();
	  
	  String lancamentoJson = mapper.writeValueAsString(lancamentoMap);
	  
	  mockMvc.perform(post("/lancamento")
	              .contentType(MediaType.APPLICATION_JSON_UTF8)
	              .content(lancamentoJson))
	  			.andExpect(status().isUnprocessableEntity());
  }
}
