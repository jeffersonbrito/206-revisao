package br.itau.com.cartoes.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.itau.com.cartoes.models.Fatura;
import br.itau.com.cartoes.models.Lancamento;
import br.itau.com.cartoes.services.FaturaService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = FaturaController.class)
public class FaturaControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private FaturaService faturaService;

	private String numeroCartao = "1234156718901230";
	private Fatura fatura;
	
	@Before
	public void preparar() {
		List<Lancamento> lancamentos = new ArrayList<>();
		
		Lancamento lancamento = new Lancamento();
		lancamento.setValor(new BigDecimal(5));
		lancamento.setDescricao("Compra 1");
		lancamentos.add(lancamento);
		
		lancamento = new Lancamento();
		lancamento.setValor(new BigDecimal(15));
		lancamento.setDescricao("Compra 2");
		lancamentos.add(lancamento);
		
		fatura = new Fatura();
		
		fatura.setLancamentos(lancamentos);
		fatura.setTotal(new BigDecimal(20));
		
	}
	
	@Test
	@WithMockUser(username = "1")
	public void deveCriarUmaFatura() throws Exception {
		
		when(faturaService.buscar(Mockito.anyString())).thenReturn(fatura);

		mockMvc.perform(get("/fatura/" + numeroCartao))
				.andExpect(status().isOk())
				.andExpect(content().string(containsString("total")))
				.andExpect(content().string(containsString("lancamentos")));
		
	}

}
