package br.itau.com.cartoes.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.itau.com.cartoes.dtos.Login;
import br.itau.com.cartoes.dtos.RespostaLogin;
import br.itau.com.cartoes.models.Cliente;
import br.itau.com.cartoes.security.JwtTokenProvider;
import br.itau.com.cartoes.services.ClienteService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ClienteController.class)
public class ClienteControllerTest {
  @Autowired
  private MockMvc mockMvc;
  
  @MockBean
  private ClienteService clienteService;
  @MockBean
  private JwtTokenProvider tokenProvider;
  
  private ObjectMapper mapper = new ObjectMapper();
  
  private Cliente cliente = new Cliente();
  private String cpfValido = "792.699.113-90";
  
  @Before
  public void preparar() {
    cliente = new Cliente();
    
    cliente.setNome("José Cliente");
    cliente.setCpf(cpfValido);
    cliente.setSenha("umaSenha123");
  }
  
  @Test
  public void deveCriarUmCliente() throws Exception {
    when(clienteService.criar(any(Cliente.class))).thenReturn(cliente);
    
    String clienteJson = mapper.writeValueAsString(cliente);
    
    mockMvc.perform(post("/cliente")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(clienteJson))
        .andExpect(status().isCreated())
        .andExpect(content().string(clienteJson));
  }
  
  @Test
  public void deveValidarDadosDoCliente() throws Exception {
    Cliente cliente = new Cliente();
    cliente.setCpf("123.123.123-12");
    
    String clienteJson = mapper.writeValueAsString(cliente);
    
    mockMvc.perform(post("/cliente")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(clienteJson))
        .andExpect(status().is4xxClientError())
        .andExpect(content().string(containsString("cpf")))
        .andExpect(content().string(containsString("senha")))
        .andExpect(content().string(containsString("nome")));
  }
  
  @Test
  public void deveLogarUmCliente() throws Exception {
    cliente.setId(13);
    
    String token = "umTokenJwt";
    String idUsuario = String.valueOf(cliente.getId());
    
    Login login = new Login();
    login.setCpf(cpfValido);
    login.setSenha("umaSenha123");
    
    RespostaLogin resposta = new RespostaLogin();
    resposta.setCliente(cliente);
    resposta.setToken(token);
    
    when(clienteService.login(any(Login.class))).thenReturn(Optional.of(cliente));
    when(tokenProvider.criarToken(idUsuario)).thenReturn(token);
    
    String loginJson = mapper.writeValueAsString(login);
    String respostaJson = mapper.writeValueAsString(resposta);
    
    mockMvc.perform(post("/login")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(loginJson))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString(respostaJson)))
        .andExpect(content().string(containsString("token")));
  }
  
  @Test
  public void deveRetornarErroEmLoginInvalido() throws Exception {
    Login login = new Login();
    login.setCpf(cpfValido);
    login.setSenha("umaSenha123");
    
    when(clienteService.login(any(Login.class))).thenReturn(Optional.empty());
    
    String loginJson = mapper.writeValueAsString(login);
    
    mockMvc.perform(post("/login")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(loginJson))
        .andExpect(status().isForbidden());
  }
  
  @Test
  public void deveValidarDadosDeLogin() throws Exception {
    Login login = new Login();
    login.setCpf("123.123.123-12");
    
    String loginJson = mapper.writeValueAsString(login);
    
    mockMvc.perform(post("/login")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(loginJson))
        .andExpect(status().is4xxClientError())
        .andExpect(content().string(containsString("cpf")))
        .andExpect(content().string(containsString("senha")));
  }
}
