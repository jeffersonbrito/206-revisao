package br.itau.com.cartoes.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.itau.com.cartoes.dtos.Login;
import br.itau.com.cartoes.exceptions.ValidacaoException;
import br.itau.com.cartoes.models.Cliente;
import br.itau.com.cartoes.repositories.ClienteRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ClienteService.class)
public class ClienteServiceTest {
  @Autowired
  private ClienteService sujeito;
  
  @MockBean
  private ClienteRepository clienteRepository;
  @MockBean
  private BCryptPasswordEncoder passwordEncoder;
  
  private Cliente cliente;
  private Login login;
  private String cpf = "123.123.123-12";
  private String senha = "minhaSenha123";
  private String senhaCriptografada = "senhaCriptografada";
  
  @Before
  public void preparar() {
    cliente = new Cliente();
    cliente.setNome("José Cliente");
    cliente.setCpf(cpf);
    cliente.setSenha(senha);
    
    login = new Login();
    login.setCpf(cpf);
    login.setSenha(senha);
  }
  
  @Test
  public void deveCriarUmCliente() {
    when(clienteRepository.save(cliente)).thenReturn(cliente);
    when(passwordEncoder.encode(cliente.getSenha())).thenReturn(senhaCriptografada);
    
    Cliente clienteCriado = sujeito.criar(cliente);
    
    assertEquals(cliente.getNome(), clienteCriado.getNome());
    assertEquals(cpf, clienteCriado.getCpf());
    assertEquals(senhaCriptografada, clienteCriado.getSenha());
  }
  
  @Test(expected = ValidacaoException.class)
  public void deveLancaExcessaoQuandoTentarCriarClienteComIdDefinido() {
    cliente.setId(10);
    
    sujeito.criar(cliente);
  }
  
  @Test
  public void deveBuscarUmClientePorId() {
    int id = 1;
    cliente.setId(id);
    
    when(clienteRepository.findById(id)).thenReturn(Optional.of(cliente));
    
    Optional<Cliente> optional = sujeito.buscar(id);
    
    assertEquals(cliente, optional.get());
  }
  
  @Test
  public void deveFazerLoginDeUmCliente() {   
    cliente.setSenha(senhaCriptografada);
    
    when(clienteRepository.findByCpf(cpf)).thenReturn(Optional.of(cliente));
    when(passwordEncoder.matches(senha, senhaCriptografada)).thenReturn(true);
    
    Optional<Cliente> optional = sujeito.login(login);
    
    assertTrue(optional.isPresent());
    assertEquals(cliente, optional.get());
  }
  
  @Test
  public void deveFazerRetornarVazioEmErroDeVerificacaoDeSenha() {
    cliente.setSenha(senhaCriptografada);
    
    when(clienteRepository.findByCpf(cpf)).thenReturn(Optional.of(cliente));
    when(passwordEncoder.matches(senha, senhaCriptografada)).thenReturn(false);
    
    Optional<Cliente> optional = sujeito.login(login);
    
    assertFalse(optional.isPresent());
  }
  
  @Test
  public void deveFazerRetornarVazioCasoCpfNaoSejaEncontrado() {
    cliente.setSenha(senhaCriptografada);
    
    when(clienteRepository.findByCpf(cpf)).thenReturn(Optional.empty());
    
    Optional<Cliente> optional = sujeito.login(login);
    
    assertFalse(optional.isPresent());
  }
}
