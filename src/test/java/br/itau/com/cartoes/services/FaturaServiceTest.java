package br.itau.com.cartoes.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.itau.com.cartoes.models.Fatura;
import br.itau.com.cartoes.models.Lancamento;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = FaturaService.class)
public class FaturaServiceTest {
	
	@Autowired
	private FaturaService faturaService;
	
	@MockBean
	private LancamentoService lancamentoService;
	
	private List<Lancamento> lancamentos;
	private String numeroCartao = "1234156718901230";
	
	@Before
	public void preparar() {
		lancamentos = new ArrayList<>();
		
		Lancamento lancamento = new Lancamento();
		lancamento.setValor(new BigDecimal(5));
		lancamento.setDescricao("Compra 1");
		lancamentos.add(lancamento);
		
		lancamento = new Lancamento();
		lancamento.setValor(new BigDecimal(15));
		lancamento.setDescricao("Compra 2");
		lancamentos.add(lancamento);
		
	}
	
	@Test
	@WithMockUser(username = "1")
	public void deveCriarUmaFatura() {
		
		when(lancamentoService.buscarTodasPorNumeroDoCartao(anyString())).thenReturn(lancamentos);
		
		Fatura fatura = faturaService.buscar(numeroCartao);
		
		assertEquals(fatura.getLancamentos().spliterator().getExactSizeIfKnown(), 2l);
		assertEquals(fatura.getTotal(), new BigDecimal(20));
	}

}
