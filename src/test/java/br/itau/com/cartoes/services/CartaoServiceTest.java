package br.itau.com.cartoes.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.itau.com.cartoes.exceptions.ValidacaoException;
import br.itau.com.cartoes.models.Bandeira;
import br.itau.com.cartoes.models.Cartao;
import br.itau.com.cartoes.models.Cliente;
import br.itau.com.cartoes.repositories.CartaoRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = CartaoService.class)
public class CartaoServiceTest {
  @Autowired
  private CartaoService sujeito;
  
  @MockBean
  private CartaoRepository cartaoRepository;
  @MockBean
  private ClienteService clienteService;
  
  private Cartao cartao;
  private Cliente cliente;
  private Bandeira bandeira;
  
  @Before
  public void preparar() {
    bandeira = Bandeira.MASTERCARD;
    
    cliente = new Cliente();
    cliente.setId(1);
    
    cartao = new Cartao();
    cartao.setCliente(cliente);
    cartao.setAtivo(false);
    cartao.setBandeira(bandeira);
  }
  
  @Test
  public void deveCriarUmCartao() {
    when(clienteService.buscar(cliente.getId())).thenReturn(Optional.of(cliente));
    when(cartaoRepository.findById(anyString())).thenReturn(Optional.empty());
    when(cartaoRepository.save(any(Cartao.class))).then(argument -> argument.getArgument(0));
    
    Cartao cartaoCriado = sujeito.criar(cartao);
    
    assertNotNull(cartaoCriado.getNumero());
    assertEquals(16, cartaoCriado.getNumero().length());
    assertFalse(cartaoCriado.getAtivo());
    assertEquals(bandeira, cartaoCriado.getBandeira());
    assertEquals(cliente, cartaoCriado.getCliente());
  }
  
  @Test
  public void deveGerarOutroNumeroEmCasoDeRepeticao() {
    when(clienteService.buscar(cliente.getId())).thenReturn(Optional.of(cliente));
    
    when(cartaoRepository.findById(anyString()))
      .thenReturn(Optional.of(cartao))
      .thenReturn(Optional.empty());
    
    when(cartaoRepository.save(any(Cartao.class))).then(argument -> argument.getArgument(0));
    
    sujeito.criar(cartao);
    
    verify(cartaoRepository, times(2)).findById(anyString());
  }
  
  @Test
  public void deveBuscarUmCartao() {
    when(cartaoRepository.findById(cartao.getNumero())).thenReturn(Optional.of(cartao));
    
    Optional<Cartao> optional = sujeito.buscar(cartao.getNumero());
    
    assertEquals(cartao, optional.get());
  }
  
  @Test(expected = ValidacaoException.class)
  public void deveLancarExcessaoAoCriarCartaoDeClienteInexistente() {
    when(clienteService.buscar(cliente.getId())).thenReturn(Optional.empty());
    
    sujeito.criar(cartao);
  }
  
  @Test
  public void deveAtivarUmCartao() {
    when(cartaoRepository.findById(cartao.getNumero())).thenReturn(Optional.of(cartao));
    
    sujeito.ativar(cartao.getNumero());
    
    verify(cartaoRepository).save(any(Cartao.class));
  }
  
  @Test(expected = ValidacaoException.class)
  public void deveLancarExcessaoAoAtivarUmCartaoInexistente() {
    when(cartaoRepository.findById(cartao.getNumero())).thenReturn(Optional.empty());
    
    sujeito.ativar(cartao.getNumero());
  }
  
  @Test(expected = ValidacaoException.class)
  public void deveLancarExcessaoAoAtivarUmCartaoJaAtivo() {
    cartao.setAtivo(true);
    
    when(cartaoRepository.findById(cartao.getNumero())).thenReturn(Optional.of(cartao));
    
    sujeito.ativar(cartao.getNumero());
  }
}
